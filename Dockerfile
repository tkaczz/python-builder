FROM builder:latest
LABEL author="illuminati_systemd_nwo"

ARG user_name=builder
ARG build_folder=build

# pre build steps
COPY ./install_deps.sh ./
RUN chmod +x ./install_deps.sh && \
    ./install_deps.sh

COPY ./python_build.sh /home/${user_name}/
RUN chmod +x /home/${user_name}/python_build.sh && \
    chown ${user_name}:${user_name} /home/${user_name}/python_build.sh && \
    mkdir /home/${user_name}/${build_folder} && \
    chown -R ${user_name}:${user_name} /home/${user_name}/${build_folder}

# env setup
USER ${user_name}
WORKDIR /home/${user_name}

ENTRYPOINT ["bash", "./python_build.sh"]
