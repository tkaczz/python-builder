apt-get build-dep -y python3.5 &&
apt-get install -y \
    libreadline6-dev \
    libsqlite3-dev \
    liblzma-dev \
    libbz2-dev \
    tk8.5-dev \
    blt-dev \
    libgdbm-dev \
    libssl-dev \
    libncurses5-dev