#!/bin/bash
# based on https://docs.python.org/devguide/setup.html
# and https://zeth.net/2014/01/11/building_python_from_source_on_debian_or_ubuntu.html

TYPE=$1
VERSION=$2

function build_static {
    prepare_sources
    setup_env
    ./configure LDFLAGS="-static" --disable-shared --prefix=$HOME/build-env
    make LDFLAGS="-static" LINKFORSHARED=" "
    make install
    pack_build
}

function build_dynamic {
    prepare_sources
    setup_env
    ./configure --with-pydebug --prefix=$HOME/build-env
    make
    make install
    pack_build
}

function prepare_sources {
    wget https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz
    tar xvf Python-$VERSION.tar.xz
}

function setup_env {
    mkdir $HOME/build-env
    cd $HOME/Python-$VERSION
}

function pack_build {
    cd $HOME/build-env
    tar cvfJ python-$VERSION.tar.xz ./*
    mv python-$VERSION.tar.xz $HOME/build
}

if [ -z $TYPE ]; then
    echo "Please provide build type, 'dynamic' or 'static'"
    exit 1
fi

if [ $TYPE == "static" ]; then
    build_static
elif [ $TYPE == "dynamic" ]; then
    build_dynamic
fi

# check if version arg is provided
if [ -z $VERSION ]; then
    echo "Please provide python version"
    exit 1
fi