Clone this repo using git

> git clone https://bitbucket.org/tkaczz/python-builder

Switch directory
> cd ./python-builder/

To build container run

> docker build -t python-builder:latest .

After that you can use this container, just run (for example)

> docker run -v $PWD/build:/home/builder/build -it --rm python-builder:latest dynamic 3.6.3
